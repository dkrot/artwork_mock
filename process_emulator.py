from cache import *
from time import time
import image_convertor


class ImageProcessEmulator:
    CONVERTING_PROGRESS_STARTED = 1
    CONVERTING_STATUS_IN_PROGRESS = 2
    CONVERTING_STATUS_DONE = 3
    CONVERTING_STATUS_IMAGE_NOT_FOUND = 4

    def __init__(self, cfg):
        self.mem_cache = CacheInventory()
        self.cfg = cfg

    def put_image(self, token, image):
        self.mem_cache.add(CachedObject(token, image, self.cfg.cache_ttl))

    def get_processed_image(self, token):
        return self.mem_cache.get(ImageProcessEmulator.processed_key(token))

    @staticmethod
    def processed_key(token):
        return 'processed:' + token

    def process_image(self, token, style):
        img = self.mem_cache.get(token)
        if img:
            new_image = image_convertor.convert_jpeg(img)
            self.mem_cache.add(CachedObject(ImageProcessEmulator.processed_key(token),
                                            new_image, self.cfg.cache_converted_ttl))
            return ImageProcessEmulator.CONVERTING_PROGRESS_STARTED

        return ImageProcessEmulator.CONVERTING_STATUS_IMAGE_NOT_FOUND

    def check_status(self, token):
        key = ImageProcessEmulator.processed_key(token)
        converted_image = self.mem_cache.get(key)

        if converted_image:
            inf = self.mem_cache.info(key)
            if time() >= inf.timestamp + self.cfg.process_time:
                return ImageProcessEmulator.CONVERTING_STATUS_DONE
            return ImageProcessEmulator.CONVERTING_STATUS_IN_PROGRESS

        return ImageProcessEmulator.CONVERTING_STATUS_IMAGE_NOT_FOUND
