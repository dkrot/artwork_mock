import hashlib
import unittest

SALT_SIGNATURE = 'Good artists copy, great artists steal'
SALT_TOKEN = 'V3|-R{e[|iH$[kO}{|F4jw1"j[ux."'


def calc_signature(image):
    h = hashlib.sha1()
    h.update(image + SALT_SIGNATURE)
    return h.hexdigest()


def calc_token(image):
    h = hashlib.sha1()
    h.update(image + SALT_TOKEN)
    return h.hexdigest()


class TestToken(unittest.TestCase):
    def test_signature(self):
        self.assertEqual(calc_signature('AAA'), '3df68a85b65bdcf2ff59216c173b22a2e89b2ac3')


if __name__ == '__main__':
    unittest.main()
