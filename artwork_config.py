import ConfigParser


class ArtworkConfig:
    def __init__(self, path):
        self.config = ConfigParser.RawConfigParser()
        self.config.read(path)

        self.listen_port = self.config.getint('server', 'port')
        self.listen_host = self.config.get('server', 'host')

        self.cache_dir = self.config.get('cache', 'directory')
        self.cache_ttl = self.config.getint('cache', 'ttl')
        self.cache_converted_ttl = self.config.getint('cache', 'converted_ttl')

        self.process_time = self.config.getint('convert', 'time')
        self.process_directory = self.config.get('convert', 'result_dir')
        self.result_url_prefix = self.config.get('convert', 'result_url_prefix')
