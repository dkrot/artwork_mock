from PIL import Image
import PIL.ImageOps
import cStringIO


def convert_jpeg(image):
    in_file = cStringIO.StringIO(image)
    image = Image.open(in_file)
    inverted_image = PIL.ImageOps.invert(image)

    out_file = cStringIO.StringIO()
    inverted_image.save(out_file, 'JPEG', quality=80)
    return out_file.getvalue()
