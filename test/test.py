#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import unittest
import requests
import json
import time

import sys
sys.path.append('..')
import image_token

HOST_NAME = 'https://artisto.dkrot.pro'
ADDR_PREFIX = HOST_NAME + '/v1'


class TestProcessing(unittest.TestCase):
    def compareWithFile(self, content, fname):
        orig_content = open(fname).read()
        self.assertEqual(content, orig_content)

    def assert_ok_json(self, response):
        self.assertNotIn('error', response)
        self.assertIn('result', response)

    def test_full_processing(self):
        files = {
            'image': open('test_image.jpg', 'rb'),
            'signature': image_token.calc_signature(open('test_image.jpg', 'rb').read())
        }
        r = requests.post(ADDR_PREFIX + '/upload', files=files)
        self.assertEqual(r.status_code, 200)
        response = json.loads(r.text)
        self.assert_ok_json(response)
        self.assertIn('id', response['result'])
        req_id = response['result']['id']

        r = requests.get(ADDR_PREFIX + '/process', params={'id': req_id, 'style': 'scream'})
        self.assertEqual(r.status_code, 200)
        self.assertDictEqual(json.loads(r.text), {'result': {'status': 'queued'}})

        for i in range(2):
            r = requests.get(ADDR_PREFIX + '/status', params={'id': req_id})
            self.assertEqual(r.status_code, 200)
            self.assert_ok_json(json.loads(r.text))
            self.assertDictEqual(json.loads(r.text), {'result': {'status': 'in-progress'}})
            time.sleep(1)  # simulate polling

        time.sleep(4)

        r = requests.get(ADDR_PREFIX + '/status', params={'id': req_id})
        self.assertEqual(r.status_code, 200)
        response = json.loads(r.text)
        self.assert_ok_json(response)
        self.assertDictContainsSubset({'status': 'finished'}, response['result'])
        self.assertIn('url', response['result'])

        image_url = response['result']['url']
        r = requests.get(image_url, stream=True)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.headers['Content-Type'], 'image/jpeg')
        self.compareWithFile(r.raw.read(), 'processed_image.jpg')

    def test_bad_upload_format(self):
        files = {
            'image': open('test_image.jpg', 'rb')
            # uploading w/o signature
        }

        r = requests.post(ADDR_PREFIX + '/upload', files=files)
        self.assertEqual(r.status_code, 200)
        answer = json.loads(r.text)
        self.assertDictEqual(answer, {'error': {'code': 'BAD_ARGUMENTS', 'message': 'no signature argument'}})

    def test_wrong_process_id(self):
        r = requests.get(ADDR_PREFIX + '/process', params={'id': 'aabbccddee', 'style': 'scream'})
        self.assertEqual(r.status_code, 200)
        answer = json.loads(r.text)
        self.assertDictEqual(answer, {'error': {'code': 'ID_NOT_FOUND', 'message': 'image with such id not found'}})

    def test_wrong_status_id(self):
        r = requests.get(ADDR_PREFIX + '/status?id=aabbccddee')
        self.assertEqual(r.status_code, 200)
        answer = json.loads(r.text)
        self.assertDictEqual(answer, {'error': {'code': 'ID_NOT_FOUND', 'message': 'image with such id not found'}})

    def test_wrong_upload_signature(self):
        files = {
            'image': open('test_image.jpg', 'rb'),
            'signature': 'sabracadabra'
        }

        r = requests.post(ADDR_PREFIX + '/upload', files=files)
        self.assertEqual(r.status_code, 200)
        answer = json.loads(r.text)
        self.assertDictEqual(answer, {'error': {'code': 'WRONG_SIGNATURE', 'message': 'incorrect signature value'}})

    def test_wrong_urls(self):
        files = {
            'image': open('test_image.jpg', 'rb'),
            'signature': image_token.calc_signature(open('test_image.jpg', 'rb').read())
        }

        r = requests.post(HOST_NAME + '/v2/upload', files=files)
        self.assertEqual(r.status_code, 404)

        r = requests.get(ADDR_PREFIX + '/download')
        self.assertEqual(r.status_code, 404)


if __name__ == '__main__':
    unittest.main()
