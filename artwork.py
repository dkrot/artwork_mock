#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import argparse
import logging
import BaseHTTPServer
import sys
import cgi
from artwork_config import ArtworkConfig
import image_token
import os.path
import json
import re
from process_emulator import ImageProcessEmulator
from urlparse import urlparse, parse_qs


def make_request_handler_class(a_cfg, a_emulator):
    class ArtworkRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
        cfg = a_cfg
        emulator = a_emulator

        def do_HEAD(self):
            logging.debug('HEADER %s' % self.path)
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()

        def extact_token(self):
            match = re.search(r'/v1/status/(\w+)', self.path)
            if not match:
                return None

            return match.group(1)

        def _save_result_image(self, image, token):
            fname = os.path.join(self.cfg.process_directory, token)
            fd = open(fname, "wb")
            fd.write(image)
            fd.close()

        def do_GET(self):
            action = self._get_action()

            if action == 'process':
                self.handle_process()
            elif action == 'status':
                self.handle_status()
            else:
                self.send_error(404)

        def _handle_post_data(self):
            return cgi.FieldStorage(fp=self.rfile, headers=self.headers, environ={'REQUEST_METHOD': 'POST'})

        def write_json(self, obj):
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(json.dumps(obj, sort_keys=True, indent=4, separators=(',', ': ')))

        def write_error(self, code, message, data=None):
            assert isinstance(code, str)
            details = {'code': code, 'message': message}
            if data is not None:
                details['data'] = data

            self.write_json({'error': details})

        def write_result(self, obj):
            self.write_json({'result': obj})

        def _save_image(self, image, token):
            fname = os.path.join(self.cfg.cache_dir, token)
            fd = open(fname, "wb")
            fd.write(image)
            fd.close()

        def _get_action(self):
            match = re.search(r'/v1/(\w+)', self.path)
            if not match:
                return None
            return match.group(1)

        @staticmethod
        def validate_signature(image, signature):
            our_signature = image_token.calc_signature(image)
            return our_signature == signature

        def handle_upload_image(self):
            form_data = self._handle_post_data()

            if 'image' not in form_data:
                self.write_error('BAD_ARGUMENTS', 'no image argument')
                return

            if 'signature' not in form_data:
                self.write_error('BAD_ARGUMENTS', 'no signature argument')
                return

            image = form_data.getvalue('image')
            if not ArtworkRequestHandler.validate_signature(image, form_data.getvalue('signature')):
                self.write_error('WRONG_SIGNATURE', 'incorrect signature value')
                return

            token = image_token.calc_token(image)
            logging.debug('POST %s (image %d bytes, token=%s)' % (self.path, len(image), token))
            self._save_image(image, token)
            self.emulator.put_image(token, image)
            self.write_result({'id': token})

        def handle_process(self):
            params = parse_qs(urlparse(self.path).query)
            res = self.emulator.process_image(params['id'][0], params['style'][0])
            if res == ImageProcessEmulator.CONVERTING_PROGRESS_STARTED:
                self.write_result({'status': 'queued'})
            else:
                self.write_error('ID_NOT_FOUND', 'image with such id not found')

        def handle_status(self):
            params = parse_qs(urlparse(self.path).query)
            token = params['id'][0]

            logging.debug('GET %s (token=%s)' % (self.path, token))

            res = self.emulator.check_status(token)

            if res == ImageProcessEmulator.CONVERTING_STATUS_DONE:
                self._save_result_image(self.emulator.get_processed_image(token), token)
                url = '%s/%s' % (self.cfg.result_url_prefix, token)
                self.write_result({'status': 'finished', 'url': url})
            elif res == ImageProcessEmulator.CONVERTING_STATUS_IN_PROGRESS:
                self.write_result({'status': 'in-progress'})
            else:
                assert res == ImageProcessEmulator.CONVERTING_STATUS_IMAGE_NOT_FOUND
                self.write_error('ID_NOT_FOUND', 'image with such id not found')

        def do_POST(self):
            logging.debug('POST %s' % self.path)
            action = self._get_action()

            if action == 'upload':
                self.handle_upload_image()
            else:
                self.send_error(404)

    return ArtworkRequestHandler


def init_logger(verbosity):
    logging.basicConfig(format='[%(asctime)s] %(levelname).3s: %(message)s', datefmt='%d.%m.%Y %H:%M:%S')
    if verbosity > 0:
        logging.getLogger().setLevel(logging.INFO if verbosity == 1 else logging.DEBUG)


def parse_command_line():
    parser = argparse.ArgumentParser(description='Grab new emails and automatically check assignments')
    parser.add_argument('-c', '--config', metavar='config_file', type=str, required=True, help='path to config file')
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='be verbose (-vv increase it more)')
    return parser.parse_args()


def launch_server(cfg):
    emulator = ImageProcessEmulator(cfg)
    handler = make_request_handler_class(cfg, emulator)
    logging.info('Server starting at %s:%s' % (cfg.listen_host, cfg.listen_port))
    server = BaseHTTPServer.HTTPServer((cfg.listen_host, cfg.listen_port), handler)

    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass

    server.server_close()
    logging.info('Server stopping')


def err_exit(message):
    print >>sys.stderr, message
    sys.exit(1)


def check_config(cfg):
    if not os.path.exists(cfg.cache_dir):
        err_exit('directory "%s" not exists' % cfg.cache_dir)
    if not os.path.exists(cfg.process_directory):
        err_exit('directory "%s" not exists' % cfg.process_directory)


def main():
    opts = parse_command_line()
    init_logger(opts.verbose)
    cfg = ArtworkConfig(opts.config)
    check_config(cfg)

    launch_server(cfg)


if __name__ == '__main__':
    main()
